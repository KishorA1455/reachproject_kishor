import fetchReducer from "./fetchReducer";
import { combineReducers } from "redux";
import statusReducer from "./statusReducer";

const RootReducer = combineReducers({
	payments: fetchReducer,
	status: statusReducer,
});

export default RootReducer;
