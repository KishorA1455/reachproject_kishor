export default (state = [], action) => {
	switch (action.type) {
		case "STATUS_SUCCESS":
			console.log("STATUS_SUCCESS in Status Reducer.js");
			return { ...state, entities: action.payload };

		case "STATUS_ERROR":
			console.log("STATUS_ERROR in Status Reducer.js");
			return { ...state, entities: action.payload };

		default:
			return state;
	}
};
