const initialState = { entities: [] };

export default (state = initialState, action) => {
	switch (action.type) {
		case "FETCH_SUCCESS":
			console.log("FETCH_SUCCESS is successful");
			return { ...state, entities: action.payload };

		case "ADD_SUCCESS":
			console.log("ADD_SUCCESS is successful");
			return { ...state };

		case "FETCH_ERROR":
			return { ...state, entities: [], error: action.payload };

		case "ADD_ERROR":
			return { ...state, error: action.payload };

		default:
			return state;
	}
};
