import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { statusApi } from "../actions/actions";

const About = () => {
	const s = useSelector((state) => state.status.entities);

	const [stat, setStat] = useState("");
	const dispatch = useDispatch();

	dispatch(statusApi())
		.then((msg) => {
			console.log("About dispatch is success");
			setStat(msg);
		})
		.catch(() => {
			console.log("About dispatch failed");
		})
		.finally(() => {
			console.log("About dispatch complete");
		});

	return (
		<div style={{ paddingTop: "50px", textAlign: "center" }}>
			<h3>This is API status page : {s}</h3>
		</div>
	);
};

export default About;
