import React, { useState, useEffect } from "react";
import { fetchPayments, addPayments } from "../actions/actions";
import { useDispatch } from "react-redux";
import "../styles/styles.css";
import CreateForm from "./CreatePaymentForm";
import PaymentListing from "./PaymentListing";
import Navigate from "./Navigate";
import {
	BrowserRouter as Router,
	Link,
	NavLink,
	Switch,
	Route,
} from "react-router-dom";
import About from "./About";
import Banner from "./Banner";

const App = () => {
	const dispatch = useDispatch();
	const [pays, setPays] = useState(false);

	useEffect(() => {
		dispatch(fetchPayments());
	}, [pays, fetchPayments]);

	return (
		<div>
			<Router>
				<div className="app">
					<Navigate />
					<Switch>
						<Route exact path="/">
							<Banner />
							<PaymentListing pays={pays} setPays={setPays} />
						</Route>
						<Route path="/add">
							<CreateForm />
						</Route>
						<Route path="/about">
							<About />
						</Route>
					</Switch>
				</div>
			</Router>
		</div>
	);
};

export default App;
