import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { addPayments } from "../actions/actions";

const CreateForm = ({ pays, setPays }) => {
	const dispatch = useDispatch();
	const history = useHistory();

	const [amt, setAmt] = useState("");
	const [cid, setCid] = useState("");
	const [id, setId] = useState("");
	const [type, setType] = useState("");

	const saveRecord = async (e) => {
		e.preventDefault();
		// console.log(amt, cid, id, type);
		dispatch(
			addPayments({
				amount: amt,
				custId: cid,
				id: id,
				type: type,
			})
		)
			.then(() => {
				console.log("Add payment is successful");
				setPays(!pays);
			})
			.catch(() => {
				console.log("An error occurred while saving record in CreateForm.js");
			})
			.finally(() => {
				console.log("addPayments thunk function is completed");
				history.push("/");
			});
	};
	// amount: 6000
	// custId: 15
	// id: 14
	// paymentDate: "2021-10-11T07:24:46.000+00:00"
	// type:
	return (
		<div className="container" style={{ marginTop: 10, marginBottom: 150 }}>
			<form onSubmit={saveRecord} autoComplete="off">
				<div className="form-row">
					<div className="form-group col-md-5">
						<label htmlFor="Amount">Amount :</label>
						<input
							type="number"
							id="amtTxt"
							className="form-control"
							value={amt}
							onChange={(e) => setAmt(e.target.value)}
						/>
					</div>
					<div className="form-group col-md-5">
						<label htmlFor="CustomerId">Customer Id :</label>
						<input
							type="number"
							id="custIdTxt"
							className="form-control"
							value={cid}
							onChange={(e) => setCid(e.target.value)}
						/>
					</div>
					<div className="form-group col-md-5">
						<label htmlFor="Id">Id :</label>
						<input
							type="number"
							id="idTxt"
							className="form-control"
							value={id}
							onChange={(e) => setId(e.target.value)}
						/>
					</div>
					<div className="form-group col-md-5">
						<label htmlFor="PaymentType">Payment Type :</label>
						<input
							type="text"
							id="payTypeTxt"
							className="form-control"
							value={type}
							onChange={(e) => setType(e.target.value)}
						/>
					</div>
					<div className="form-group col-md-5">
						<input
							type="submit"
							value="Create Record"
							className="btn btn-outline-secondary"
						></input>
						<button className="btn btn-outline-secondary">Clear</button>
					</div>
				</div>
			</form>
		</div>
	);
};

export default CreateForm;
