import React from "react";
import { NavLink } from "react-router-dom";

const Navigate = () => {
	return (
		<ul className="nav">
			<li className="nav-item">
				<NavLink
					exact
					to="/"
					className="nav-link text-secondary"
					activeClassName="app"
				>
					Home
				</NavLink>
			</li>
			<li className="nav-item">
				<NavLink
					to="/add"
					className="nav-link text-secondary"
					activeClassName="app"
				>
					Add Payment
				</NavLink>
			</li>
			<li>
				<NavLink
					to="/about"
					className="nav-link text-secondary"
					activeClassName="app"
				>
					About
				</NavLink>
			</li>
		</ul>
	);
};

export default Navigate;
