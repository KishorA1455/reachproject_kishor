import React from "react";

const Payment = ({ cid, type, amt, dt, id }) => {
	return (
		<div>
			<h3>Customer id {cid} Record</h3>
			<div className="col-md-4">
				<div className="card mb-4 box-shadow" style={{ width: "18rem" }}>
					<div className="card-body">
						Customer Id : {cid} <br />
						Account Type : {type} <br />
						Amount : {amt} <br />
						Payment Date : {dt} <br />
						Id : {id} <br />
					</div>
				</div>
			</div>
		</div>
	);
};

export default Payment;
