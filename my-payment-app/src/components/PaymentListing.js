import { useSelector } from "react-redux";
import Payment from "./Payment";
import React from "react";

const PaymentListing = () => {
	const payments = useSelector((state) => state.payments.entities);
	console.log("payment listing.js -> payments : ", payments);

	return (
		<div>
			<h3>Payments List : </h3>
			<div className="container">
				<div className="row">
					{payments.map((i) => (
						<Payment
							key={i.id}
							id={i.id}
							cid={i.custId}
							type={i.type}
							amt={i.amount}
							dt={i.paymentDate}
						/>
					))}
				</div>
			</div>
		</div>
	);
};

export default PaymentListing;
