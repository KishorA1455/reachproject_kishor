const Banner = () => {
	return (
		<section
			className="jumbotron text-center"
			style={{ backgroundColor: "white" }}
		>
			<div className="container">
				<h1 className="jumbotron-heading">Welcome to Payments App</h1>
				<p className="lead text-muted" style={{ fontSize: "medium" }}>
					This is a simple Payment details app.
				</p>
			</div>
		</section>
	);
};

export default Banner;
