import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import RootReducer from "./reducers/rootReducer";
import thunkMiddleware from "redux-thunk";

const store = createStore(RootReducer, applyMiddleware(thunkMiddleware));
console.log("store data : ", store.getState());

ReactDOM.render(
	<Provider store={store}>
		<div>
			<h2>Welcome page - Payments App</h2>
			<App />
		</div>
	</Provider>,
	document.getElementById("root")
);
