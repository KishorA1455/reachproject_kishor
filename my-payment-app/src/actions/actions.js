import axios from "axios";

export const fetchBegin = () => {
	return {
		type: "FETCH_BEGIN",
	};
};

export const fetchSuccess = (payments) => {
	return {
		type: "FETCH_SUCCESS",
		payload: payments,
	};
};

export const fetchError = (err) => {
	return {
		type: "FETCH_ERROR",
		payload: { message: "Error occurred" },
	};
};

export const fetchPayments = () => {
	return (dispatch, getState) => {
		dispatch(fetchBegin());
		axios.get("http://localhost:8080/paymentapi/findall").then(
			(res) => {
				dispatch(fetchSuccess(res.data));
				console.log("From fetchPayments() of action.js", getState());
			},
			(err) => {
				dispatch(fetchError(err));
				console.log("Error occurred");
			}
		);
	};
};

export const addBegin = () => {
	return {
		type: "ADD_BEGIN",
	};
};

export const addSuccess = () => {
	return {
		type: "ADD_SUCCESS",
	};
};

export const addError = (err) => {
	return {
		type: "ADD_ERROR",
		payload: { message: "Error occurred while adding Payments" },
	};
};

export const addPayments = (pay) => {
	return (dispatch, getState) => {
		return axios.post("http://localhost:8080/paymentapi/save", pay).then(
			() => {
				dispatch(addSuccess());
				console.log("addPayment success", getState());
			},
			(err) => {
				dispatch(addError(err));
				console.log("addError triggered", getState());
			}
		);
	};
};

export const statusSuccess = (msg) => {
	return {
		type: "STATUS_SUCCESS",
		payload: msg,
	};
};

export const statusError = (err) => {
	return {
		type: "STATUS_ERROR",
		payload: { message: "Error occurred during the call" },
	};
};

export const statusApi = () => {
	return (dispatch, getState) => {
		return axios.get("http://localhost:8080/paymentapi/status").then(
			(res) => {
				dispatch(statusSuccess(res.data));
				console.log("API Status checked : ", res.data);
			},
			(err) => {
				dispatch(statusError(err));
				console.log("Error occurred during API Status check");
			}
		);
	};
};
